"""Test binary responses"""
import io

from awsgi2.base import StartResponse


def test_response_base64_content_type() -> None:
    """Make sure image is b64 encoded"""
    sreq = StartResponse(base64_content_types={"image/png"})
    sreq("200 OK", [("Content-Type", "image/png")])
    output = io.BytesIO(b"\x89PNG\r\n\x1a\n\x00\x00\x00\rIHDR\x00\x00\x00\xc8")

    result = sreq.response(output)
    assert result["isBase64Encoded"]
    assert result["body"] == "iVBORw0KGgoAAAANSUhEUgAAAMg="  # pragma: allowlist secret


def test_response_base64_content_encoding() -> None:
    """Make sure compressed JSON body is b64 encoded"""
    sreq = StartResponse()
    sreq("200 OK", [("Content-Type", "application/json"), ("Content-Encoding", "gzip")])
    output = io.BytesIO(
        b"\x1f\x8b\x08\x00(=Mc\x02\xff\xaaV\xcaH\xcd\xc9\xc9W\xb2RP*\xcf/\xcaIQ\xaa\x05\x00\x00\x00"
        + b'\xff\xff\x03\x00"\xae\xa3\x86\x12\x00\x00\x00'
    )

    result = sreq.response(output)
    assert result["isBase64Encoded"]
    assert result["body"] == "H4sIACg9TWMC/6pWykjNyclXslJQKs8vyklRqgUAAAD//wMAIq6jhhIAAAA="  # pragma: allowlist secret
