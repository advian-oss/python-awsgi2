"""Test the ELB/APIGW implementations"""
import io

from aws_lambda_powertools.utilities.typing import LambdaContext

from awsgi2.impl import StartResponseELB, StartResponseGW, select_impl
from awsgi2.wsgienv import environ

from .conftest import get_event_for_path


def test_impl_selection_elb() -> None:
    """Test that ELB selection works"""
    event = {
        "requestContext": {
            "elb": {"targetGroupArn": "arn:aws:elasticloadbalancing:us-east-2:0123456789:targetgroup/spam/eggs"}
        },
        "httpMethod": "GET",
        "path": "/",
        "multiValueQueryStringParameters": {},
        "multiValueHeaders": {"user-agent": ["ELB-HealthChecker/2.0"]},
        "body": "",
        "isBase64Encoded": False,
    }
    context = LambdaContext()
    got_environ, got_class = select_impl(event, context)
    assert environ is got_environ
    assert got_class is StartResponseELB


def test_impl_selection_gw() -> None:
    """Test that APIGW selection works"""
    event = get_event_for_path("/test/hello")
    context = LambdaContext()
    got_environ, got_class = select_impl(event, context)
    assert environ is got_environ
    assert got_class is StartResponseGW


def test_super_call() -> None:
    """
    Test StartResponseELB and StartResponseGW can response
    """
    srinst_elb = StartResponseELB()
    srinst_elb("200 OK", [("Content-Type", "text/html")])
    output = io.BytesIO()
    result = srinst_elb.response(output)
    assert result["statusCode"] == 200

    srinst_apigw = StartResponseGW()
    srinst_apigw("200 OK", [("Content-Type", "text/html")])
    output = io.BytesIO()
    result = srinst_apigw.response(output)
    assert result["statusCode"] == 200
