"""Test the WSGI environment helpers"""
from typing import Dict, Union, Sequence, Mapping, cast, Any
import io
import sys
from urllib.parse import urlencode


from aws_lambda_powertools.utilities.typing import LambdaContext

from awsgi2.wsgienv import environ

from .conftest import bytesio_are_equal


def test_environ_v1() -> None:
    """Test v1 event format"""
    event: Dict[str, Union[Mapping[str, str], str]] = {
        "version": "1.0",
        "httpMethod": "TEST",
        "path": "/test",
        "queryStringParameters": {
            "test": "✓",
        },
        "body": "test",
        "headers": {
            "X-test-suite": "testing",
            "Content-type": "text/plain",
            "Host": "test",
            "X-forwarded-for": "first, second",
            "X-forwarded-proto": "https",
            "X-forwarded-port": "12345",
        },
    }
    event["queryStringParameters"] = cast(Dict[str, str], event["queryStringParameters"])
    event["headers"] = cast(Dict[str, str], event["headers"])
    assert isinstance(event["body"], str)
    assert isinstance(event["headers"], dict)
    assert isinstance(event["headers"]["X-forwarded-for"], str)
    assert isinstance(event["queryStringParameters"], dict)
    context = LambdaContext()
    expected: Dict[str, Any] = {
        "REQUEST_METHOD": event["httpMethod"],
        "SCRIPT_NAME": "",
        "PATH_INFO": event["path"],
        "QUERY_STRING": urlencode(event["queryStringParameters"]),
        "CONTENT_LENGTH": str(len(event["body"])),
        "HTTP": "on",
        "SERVER_PROTOCOL": "HTTP/1.1",
        "wsgi.version": (1, 0),
        "wsgi.input": io.BytesIO(event["body"].encode("utf-8")),
        "wsgi.errors": sys.stderr,
        "wsgi.multithread": False,
        "wsgi.multiprocess": False,
        "wsgi.run_once": False,
        "CONTENT_TYPE": event["headers"]["Content-type"],
        "HTTP_CONTENT_TYPE": event["headers"]["Content-type"],
        "SERVER_NAME": event["headers"]["Host"],
        "HTTP_HOST": event["headers"]["Host"],
        "REMOTE_ADDR": event["headers"]["X-forwarded-for"].split(", ", maxsplit=1)[0],
        "HTTP_X_FORWARDED_FOR": event["headers"]["X-forwarded-for"],
        "wsgi.url_scheme": event["headers"]["X-forwarded-proto"],
        "HTTP_X_FORWARDED_PROTO": event["headers"]["X-forwarded-proto"],
        "SERVER_PORT": event["headers"]["X-forwarded-port"],
        "HTTP_X_FORWARDED_PORT": event["headers"]["X-forwarded-port"],
        "HTTP_X_TEST_SUITE": event["headers"]["X-test-suite"],
        "awsgi.event": event,
        "awsgi.context": context,
    }
    result = environ(event, context)
    for key, val in result.items():
        if isinstance(val, io.BytesIO):
            assert isinstance(expected[key], io.BytesIO)
            assert bytesio_are_equal(val, expected[key])
            continue
        assert val == expected[key]


def test_environ_v2() -> None:
    """
    Same test as above but with version 2.0 format where http method
    and path are nested in requestsContext.http attribute.
    """
    event: Dict[str, Union[Mapping[str, Union[Mapping[str, str], str]], str]] = {
        "version": "2.0",
        "queryStringParameters": {
            "test": "✓",
        },
        "requestContext": {
            "http": {
                "method": "TEST",
                "path": "/text",
            }
        },
        "body": "test",
        "headers": {
            "X-test-suite": "testing",
            "Content-type": "text/plain",
            "Host": "test",
            "X-forwarded-for": "first, second",
            "X-forwarded-proto": "https",
            "X-forwarded-port": "12345",
        },
    }
    event["queryStringParameters"] = cast(Mapping[str, str], event["queryStringParameters"])
    event["headers"] = cast(Mapping[str, str], event["headers"])
    assert isinstance(event["body"], str)
    assert isinstance(event["headers"], dict)
    assert isinstance(event["requestContext"], dict)
    assert isinstance(event["requestContext"]["http"], dict)
    assert isinstance(event["requestContext"]["http"]["path"], str)
    assert isinstance(event["headers"]["X-forwarded-for"], str)
    assert isinstance(event["queryStringParameters"], dict)
    context = LambdaContext()
    expected: Dict[str, Any] = {
        "REQUEST_METHOD": event["requestContext"]["http"]["method"],
        "SCRIPT_NAME": "",
        "PATH_INFO": event["requestContext"]["http"]["path"],
        "QUERY_STRING": urlencode(event["queryStringParameters"]),
        "CONTENT_LENGTH": str(len(event["body"])),
        "HTTP": "on",
        "SERVER_PROTOCOL": "HTTP/1.1",
        "wsgi.version": (1, 0),
        "wsgi.input": io.BytesIO(event["body"].encode("utf-8")),
        "wsgi.errors": sys.stderr,
        "wsgi.multithread": False,
        "wsgi.multiprocess": False,
        "wsgi.run_once": False,
        "CONTENT_TYPE": event["headers"]["Content-type"],
        "HTTP_CONTENT_TYPE": event["headers"]["Content-type"],
        "SERVER_NAME": event["headers"]["Host"],
        "HTTP_HOST": event["headers"]["Host"],
        "REMOTE_ADDR": event["headers"]["X-forwarded-for"].split(", ", maxsplit=1)[0],
        "HTTP_X_FORWARDED_FOR": event["headers"]["X-forwarded-for"],
        "wsgi.url_scheme": event["headers"]["X-forwarded-proto"],
        "HTTP_X_FORWARDED_PROTO": event["headers"]["X-forwarded-proto"],
        "SERVER_PORT": event["headers"]["X-forwarded-port"],
        "HTTP_X_FORWARDED_PORT": event["headers"]["X-forwarded-port"],
        "HTTP_X_TEST_SUITE": event["headers"]["X-test-suite"],
        "awsgi.event": event,
        "awsgi.context": context,
    }
    result = environ(event, context)
    for key, val in result.items():
        if isinstance(val, io.BytesIO):
            assert isinstance(expected[key], io.BytesIO)
            assert bytesio_are_equal(val, expected[key])
            continue
        assert val == expected[key]


def test_environ_multi_value_query_string_parameters() -> None:
    """Test list of querystring params"""
    event: Dict[str, Union[Mapping[str, Union[str, Sequence[str]]], str]] = {
        "httpMethod": "TEST",
        "path": "/test",
        "queryStringParameters": {
            "test": "✓",
        },
        "multiValueQueryStringParameters": {
            "test": ["1", "2"],
        },
        "body": "test",
        "headers": {
            "X-test-suite": "testing",
            "Content-type": "text/plain",
            "Host": "test",
            "X-forwarded-for": "first, second",
            "X-forwarded-proto": "https",
            "X-forwarded-port": "12345",
        },
    }
    event["queryStringParameters"] = cast(Mapping[str, str], event["queryStringParameters"])
    event["headers"] = cast(Mapping[str, str], event["headers"])
    event["multiValueQueryStringParameters"] = cast(
        Mapping[str, Sequence[str]], event["multiValueQueryStringParameters"]
    )
    assert isinstance(event["body"], str)
    assert isinstance(event["headers"], dict)
    assert isinstance(event["headers"]["X-forwarded-for"], str)
    assert isinstance(event["queryStringParameters"], dict)
    assert isinstance(event["multiValueQueryStringParameters"], dict)
    assert isinstance(event["multiValueQueryStringParameters"]["test"], list)
    assert isinstance(event["multiValueQueryStringParameters"]["test"][0], str)
    context = LambdaContext()
    expected: Dict[str, Any] = {
        "REQUEST_METHOD": event["httpMethod"],
        "SCRIPT_NAME": "",
        "PATH_INFO": event["path"],
        "QUERY_STRING": urlencode([("test", "1"), ("test", "2")]),
        "CONTENT_LENGTH": str(len(event["body"])),
        "HTTP": "on",
        "SERVER_PROTOCOL": "HTTP/1.1",
        "wsgi.version": (1, 0),
        "wsgi.input": io.BytesIO(event["body"].encode("utf-8")),
        "wsgi.errors": sys.stderr,
        "wsgi.multithread": False,
        "wsgi.multiprocess": False,
        "wsgi.run_once": False,
        "CONTENT_TYPE": event["headers"]["Content-type"],
        "HTTP_CONTENT_TYPE": event["headers"]["Content-type"],
        "SERVER_NAME": event["headers"]["Host"],
        "HTTP_HOST": event["headers"]["Host"],
        "REMOTE_ADDR": event["headers"]["X-forwarded-for"].split(", ", maxsplit=1)[0],
        "HTTP_X_FORWARDED_FOR": event["headers"]["X-forwarded-for"],
        "wsgi.url_scheme": event["headers"]["X-forwarded-proto"],
        "HTTP_X_FORWARDED_PROTO": event["headers"]["X-forwarded-proto"],
        "SERVER_PORT": event["headers"]["X-forwarded-port"],
        "HTTP_X_FORWARDED_PORT": event["headers"]["X-forwarded-port"],
        "HTTP_X_TEST_SUITE": event["headers"]["X-test-suite"],
        "awsgi.event": event,
        "awsgi.context": context,
    }
    result = environ(event, context)
    for key, val in result.items():
        if isinstance(val, io.BytesIO):
            assert isinstance(expected[key], io.BytesIO)
            assert bytesio_are_equal(val, expected[key])
            continue
        assert val == expected[key]


def test_paths_unquoted() -> None:
    """Test that paths will get unquoted/escaped"""
    event: Dict[str, Union[Mapping[str, str], str]] = {
        "httpMethod": "GET",
        "path": "/test%20path%20with%20spaces",
        "queryStringParameters": {},
        "body": "test",
        "headers": {
            "X-test-suite": "testing",
            "Content-type": "text/plain",
            "Host": "test",
            "X-forwarded-for": "first, second",
            "X-forwarded-proto": "https",
            "X-forwarded-port": "12345",
        },
    }
    event["queryStringParameters"] = cast(Mapping[str, str], event["queryStringParameters"])
    event["headers"] = cast(Mapping[str, str], event["headers"])
    assert isinstance(event["body"], str)
    assert isinstance(event["headers"], dict)
    assert isinstance(event["headers"]["X-forwarded-for"], str)
    context = LambdaContext()
    expected: Dict[str, Any] = {
        "REQUEST_METHOD": event["httpMethod"],
        "SCRIPT_NAME": "",
        "PATH_INFO": "/test path with spaces",
        "QUERY_STRING": "",
        "CONTENT_LENGTH": str(len(event["body"])),
        "HTTP": "on",
        "SERVER_PROTOCOL": "HTTP/1.1",
        "wsgi.version": (1, 0),
        "wsgi.input": io.BytesIO(event["body"].encode("utf-8")),
        "wsgi.errors": sys.stderr,
        "wsgi.multithread": False,
        "wsgi.multiprocess": False,
        "wsgi.run_once": False,
        "CONTENT_TYPE": event["headers"]["Content-type"],
        "HTTP_CONTENT_TYPE": event["headers"]["Content-type"],
        "SERVER_NAME": event["headers"]["Host"],
        "HTTP_HOST": event["headers"]["Host"],
        "REMOTE_ADDR": event["headers"]["X-forwarded-for"].split(", ", maxsplit=1)[0],
        "HTTP_X_FORWARDED_FOR": event["headers"]["X-forwarded-for"],
        "wsgi.url_scheme": event["headers"]["X-forwarded-proto"],
        "HTTP_X_FORWARDED_PROTO": event["headers"]["X-forwarded-proto"],
        "SERVER_PORT": event["headers"]["X-forwarded-port"],
        "HTTP_X_FORWARDED_PORT": event["headers"]["X-forwarded-port"],
        "HTTP_X_TEST_SUITE": event["headers"]["X-test-suite"],
        "awsgi.event": event,
        "awsgi.context": context,
    }
    result = environ(event, context)
    for key, val in result.items():
        if isinstance(val, io.BytesIO):
            assert isinstance(expected[key], io.BytesIO)
            assert bytesio_are_equal(val, expected[key])
            continue
        assert val == expected[key]
