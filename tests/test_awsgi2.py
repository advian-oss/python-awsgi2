"""Package level tests"""
from awsgi2 import __version__


def test_version() -> None:
    """Make sure version matches expected"""
    assert __version__ == "1.0.2"
