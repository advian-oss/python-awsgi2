"""Test the wrapper"""
from typing import Dict, Any
import logging

from flask import Flask, json
from aws_lambda_powertools.utilities.typing import LambdaContext

from awsgi2 import response
from .conftest import get_event_for_path

LOGGER = logging.getLogger(__name__)
FLASK_APP = Flask("awsgitest")


@FLASK_APP.route("/json_dict")
def ret_json_dict() -> Dict[str, Any]:
    """Return dict for JSON body"""
    return {"hello": "world"}


@FLASK_APP.route("/str_body")
def ret_str() -> str:
    """Return dict for JSON body"""
    return "<h1>Hello World</h1>"


def test_get_json() -> None:
    """Test JSON auto-encode from route that returns dict"""
    event = get_event_for_path("/json_dict")
    context = LambdaContext()
    resp = response(FLASK_APP, event, context)
    LOGGER.debug("resp={}".format(repr(resp)))
    assert isinstance(resp["body"], str)
    assert json.dumps(json.loads(resp["body"])) == json.dumps({"hello": "world"})


def test_get_str() -> None:
    """Test route that retuns straight up HTML"""
    event = get_event_for_path("/str_body")
    context = LambdaContext()
    resp = response(FLASK_APP, event, context)
    LOGGER.debug("resp={}".format(repr(resp)))
    assert isinstance(resp["body"], str)
    assert resp["body"].strip() == "<h1>Hello World</h1>"
